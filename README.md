# md-doc

`md-doc` is a markdown document parser that creates document structure from markdown syntax.
It can be read, modified and saved using a resulting document structure object. You can also define
custom extensions (see apiary-preprocessor npm module).

## Creating, reading and saving documents

    var Document = require('node-document');
    
Create empty document

    var doc = new Document();

**Document.readFile(path, callback)** - Read a document file

    Document.readFile('path/to/doc.md', function(err, document) {
        if(err){
            //Do something with error
            return;
        }
        //Do something with document
    });
    
**Document.read(sourceString, callback)** - Read a document from string

    Document.read('# Header\n\nLorem ipsum **dolor** sit *amet*\n', function(err, document){
        if(err){
            //Do something with error
            return;
        }
        //Do something with document
    });
    
Calling `read()` or `readFile()` on existing document erases it's content.

**doc.save(path, type, callback)** - Save the document (`text` for markdown, `html` for html)

    doc.save('path/to/file.md', 'text', function(err){
        //Document saved as markdown file
    });

## Structures

### Content

**getText()** - Returns content's text (including all tags inside)

**getHtml()** - Returns html content 

**find(selector)** - Returns array of content elements that are filtered by type

**findByContents(content)** - Returns array of content elements that are filtered by content

### Content Types

**Content.registerContentType(contentType, priority)**

**Content.registerContentTypes(arrayOfContentTypes, priority)**

### Get Tree

Returns full simple tree of document's (or elements) structure

## Extensions

//TODO
