'use strict';

var Content = require('./content'),
  ContentType = require('./content-type'),
  regexUtils = require('./regex.utils.js');

function section(indent) {
  return new ContentType({
    match: regexUtils.replace(
      /\n(#{_indent_}[^#].*)((?:\n|.*)*?)(?=(?:\n#{_indent_}[^#].*)|$)/
    )(/_indent_/g, String(indent))(),
    name: 'Section',
    short: 'sect',
    isBlock: true,
    validAt: ['Document', 'Section'],
    on: function (match) {
      var section = new Content(this, '\n' + match[2].trim()),
        header = match[1];
      //Prepare level and header
      section.level = 0;
      if (header !== null) {
        while (header[section.level] === '#') {
          section.level++;
        }
        section.header = header.replace(/^#+/, '').trim();
      } else {
        section.header = '';
      }

      return section;
    },
    getText: function () {
      var level = '';
      for (var i = 0; i < this.level; i++) {
        level += '#';
      }
      return '\n' + level + ' ' + this.header + '\n\n' + this.getContentText();
    },
    getHtml: function () {
      return '<h' + this.level + '>' + this.header + '</h' + this.level + '><div>' + this.getContentHtml() + '</div>';
    }
  });
}

var listPart = /\n( *)(\*|-|\+|\d+.) +(.*)/,
  listPartContinuation = /(?:\n *(?! |\*|-|\+|\d+.).+)*/,
  listElement = /_listPart_(_listPartContinuation_)(?=\n|$)/,
  list = /(?:_listPart_(_listPartContinuation_))+(?=\n\s*\n)/;

//var listPart = /\n( *)([*+-]|\d+\.) +(.*)/,
//  listPartContinuation = /(?:\n *(?![*+-]|\d+\.).+)*/,

listElement = regexUtils.replace(listElement)
(/_listPart_/g, listPart)
(/_listPartContinuation_/g, listPartContinuation)
();
list = regexUtils.replace(list)
(/_listPart_/g, listPart)
(/_listPartContinuation_/g, listPartContinuation)
();

module.exports = [
  section(1),
  section(2),
  section(3),
  section(4),
  section(5),
  section(6),
  new ContentType({
    match: [/\[(.*)?]\((.*)?\)/],
    name: 'Link',
    short: 'url',
    parseContent: true,
    on: function (match) {
      var link = new Content(this, match[1]);
      link.link = match[2];
      return link;
    },
    getText: function () {
      return '[' + this.getContentText() + '](' + this.link + ')';
    },
    getHtml: function () {
      return '<a href="' + this.link + '">' + this.getContentHtml() + '</a>'
    }
  }),
  new ContentType({
    match: [/!\[(.*)?]\((.*)?\)/],
    name: 'Image',
    short: 'img',
    parseContent: true,
    on: function (match) {
      var link = new Content(this, match[1]);
      link.image = match[2];
      return link;
    },
    getText: function () {
      return '![' + this.getContentText() + '](' + this.link + ')';
    },
    getHtml: function () {
      return '<img src="' + this.image + '" alt="' + this.getContentHtml() + '"/>';
    }
  }),
  ContentType.Simple('Strikethrough', '~~', 'strike', 'strike'),
  ContentType.Simple('Underline', '~', 'u', 'u'),
  ContentType.Simple('Bold', '__', 'b', 'b'),
  ContentType.Simple('Bold 2', '**', 'b', 'b'),
  ContentType.Simple('Italics', '_', 'i', 'i'),
  ContentType.Simple('Italics 2', '*', 'i', 'i'),
  ContentType.Simple('Code', '`', 'c', 'code').setParseContent(false),
  new ContentType({
    match: /\n```([.\n]*)\n```/,
    name: 'Code Block',
    short: 'c',
    parseContent: false,
    isBlock: true,
    on: function (match) {
      return new Content(this, match[1]);
    },
    getText: function () {
      return '\n```' + this.getContentText() + '\n```\n';
    },
    getHtml: function () {
      return '<c>' + this.getContentText() + '</c>'
    }
  }),
  new ContentType({
    match: /(?:\n>(.*))+/,
    name: 'Quote',
    short: 'qt',
    parseContent: true,
    isBlock: true,
    on: function (match) {
      var text = match[0].replace(/\n>/g, '\n');
      return new Content(this, text);
    },
    getText: function () {
      return '\n' + (this.getContentText().replace(/\n/g, '\n>')) + '\n';
    },
    getHtml: function () {
      return '<q>' + this.getContentText() + '</q>';
    }
  }),
  new ContentType({
    match: listElement,
    name: 'List Element',
    validAt: 'List',
    short: 'ul',
    parseContent: true,
    on: function (match) {
      var listElement = new Content(this, match[3] + match[4]);
      listElement.mark = match[2];
      listElement.whitespaceBefore = match[1];
      var indent = listElement.whitespaceBefore.match(/( {4})/g);
      listElement.indent = indent ? indent.length - 1 : 0;
      return listElement;
    },
    getText: function () {
      var element = this.getContentText().replace(/\s+$/, '');
      return this.whitespaceBefore + this.mark + ' ' + element + '\n';
    },
    getHtml: function () {
      return '<li>' + this.getContentHtml() + '</li>';
    }
  }),
  new ContentType({
    match: list,
    name: 'List',
    short: 'list',
    isBlock: true,
    parseContent: true,
    on: function (match) {
      var source = match[0];
      var listContent = source.replace(/^\n+/, '\n'),
        list = new Content(this, listContent);
      list.mark = listContent.match(/\*|-|\+|\d+./)[0];
      list.source = source;
      return list;
    },
    getText: function () {
      return '\n' + this.getContentText();
    },
    getHtml: function () {
      var htmlMark = this.mark.match(/\d+./) ? 'ol' : 'ul';
      return '<' + htmlMark + '>' + this.getContentHtml() + '</' + htmlMark + '>';
    }
  }),
  new ContentType({
    match: /((?:\n(?: {4})(?:.*))+)/,
    name: 'Inset',
    short: 'ins',
    parseContent: true,
    on: function (match) {
      var source = match[1].replace(/\n( {4})/g, '\n');
      return new Content(this, source);
    },
    getText: function () {
      return ('\n' + this.getContentText() + '\n').replace(/\n/g, '\n    ').replace(/(\n\s+){2,}/g, '$1');
    },
    getHtml: function () {
      return '<div style="margin-left: 20px">' + this.getContentHtml() + '</div>'
    }
  }),
  new ContentType({
    match: /a^/,
    name: 'Document',
    short: 'doc',
    on: function () {
    },
    parseContent: true
  }),
  new ContentType({
    match: /a^/,
    name: 'Text Block',
    short: 'txt',
    on: function () {
    },
    getText: function () {
      return this.contents;
    },
    parseContent: false
  })
];