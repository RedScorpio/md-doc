'use strict';

/**
 *
 * @param name {string|object}
 * @param {RegExp?} match
 * @param {function(match):Content?} onMatch
 * @param {function():string?} getText
 * @constructor
 */
var ContentType = function (name, match, onMatch, getText) {
  this.validAt = null;
  this.parseContent = true;
  this.isBlock = false;
  this.validChildren = null;
  this.after = function () {
  };
  if (typeof name === 'object' && !match && !onMatch && !getText) {
    var object = name;
    for (var prop in object) {
      if (object.hasOwnProperty(prop)) {
        this[prop] = object[prop];
      }
    }
  } else {
    this.name = name;
    this.match = match;
    this.on = onMatch;
    this.getText = getText;
  }
  if (!this.short) {
    this.short = this.name.toLowerCase().replace(/ /g, '-');
  }
};

ContentType.prototype.setMatchRegex = function (match) {
  this.match = match;
  return this;
};
ContentType.prototype.setOnMatch = function (onMatch) {
  this.on = onMatch;
  return this;
};
ContentType.prototype.setIsBlock = function (isBlock) {
  this.isBlock = isBlock;
  return this;
};
ContentType.prototype.setValidAt = function (validAt) {
  this.validAt = validAt;
  return this;
};
ContentType.prototype.setValidChildren = function (validChildren) {
  this.validChildren = validChildren;
  return this;
};
ContentType.prototype.setParseContent = function (parseContent) {
  this.parseContent = parseContent;
  return this;
};
ContentType.prototype.setAfter = function (after) {
  this.after = after;
  return this;
};
ContentType.prototype.setGetText = function (getText) {
  this.getText = getText;
  return this;
};
ContentType.prototype.setGetHtml = function (getHtml) {
  this.getHtml = getHtml;
  return this;
};

ContentType.prototype.toString = function () {
  return this.name + ' (' + this.short + ') [' + this.match.source + ']';
};

module.exports = ContentType;