'use strict';

var ContentType = require('./content-type'),
  regexUtils = require('./regex.utils.js');

function pathValidation(validAt, currentTypeName) {
  if (!validAt) {
    return true;
  }
  if (typeof validAt === 'string') {
    return validAt === currentTypeName;
  }
  for (var i = 0; i < validAt.length; i++) {
    if (validAt[i] === currentTypeName) {
      return true;
    }
  }
  return false;
}

function matchNearest(currentType, text) {
  var nearestMatch, nearest, start = text.length, end = text.length;
  for (var i = 0; i < Content.Types.length; i++) {
    var type = Content.Types[i];
    if ((!currentType.validChildren || currentType.validChildren.indexOf(type.name) >= 0) &&
      (!type.fallback || !nearestMatch) &&
      pathValidation(type.validAt, currentType.name)) {
      var match = text.match(Content.Types[i].match);
      if (match && (!nearestMatch || nearestMatch.index > match.index)) {
        nearestMatch = match;
        nearest = Content.Types[i];
        start = nearestMatch.index;
        end = start + nearestMatch[0].length;
      }
    }
  }
  return {
    match: nearestMatch,
    type: nearest,
    start: start,
    end: end
  };
}

ContentType.Simple = function (name, tag, short, htmlTag) {
  var simpleRegex = /_tag_(.*?)_tag_/;
  simpleRegex = regexUtils.replace(simpleRegex)(/_tag_/g, regexUtils.escape(tag))();
  return new ContentType({
    match: simpleRegex,
    name: name,
    short: short,
    parseContent: true,
    on: function (match) {
      return new Content(this, match[1]);
    },
    getText: function () {
      return tag + this.getContentText() + tag;
    },
    getHtml: function () {
      return '<' + htmlTag + '>' + this.getContentHtml() + '</' + htmlTag + '>';
    }
  });
};
ContentType.TypedSection = function (name, keyword, short, onHeader) {
  var Section = Content.TypesByName['Section'], keywordRegex;
  if (!keyword) {
    keywordRegex = / */;
  } else {
    keywordRegex = regexUtils.replace(/ +_keyword_ +/)(/_keyword_/g, keyword)();
  }
  var sectionRegex = regexUtils.replace(
    /\n(#{_indent_}[^#]_keywordRegex_.*)((?:\n|.*)*?)(?=(?:\n#{_indent_}[^#].*)|$)/
  )(/_keywordRegex_/g, keywordRegex)();
  var result = [];
  for (var i = 1; i <= 6; i++) {
    result.push(new ContentType({
      match: regexUtils.replace(sectionRegex)(/_indent_/g, String(i))(),
      name: name,
      short: short,
      parseContent: true,
      on: function (match) {
        var section = Section.on(match);
        section.params = section.header.replace(keywordRegex, '');
        if (this.onHeader) {
          this.onHeader.call(section, section.params);
        }
        return section;
      },
      onHeader: onHeader,
      isBlock: Section.isBlock,
      getText: Section.getText,
      getHtml: Section.getHtml
    }));
  }
  return result;
};

ContentType.ListTypedSection = function (mark, tillNext, name, keyword, short, onHeader) {
  if (!mark) {
    mark = /(?:[*+-]|\d+\.)/;
  }
  var header = /\n( *)(_mark_) +_keyword_(.*)/;
  header = regexUtils.replace(header)(/_mark_/g, mark)(/_keyword_/g, keyword)();
  var sectionRegex = /_header_\n((?:.|\s)*?)(?=(?:_ending_)|$)/;
  sectionRegex = regexUtils.replace(sectionRegex)
  (/_header_/g, header)
  (/_ending_/g, tillNext ? (tillNext instanceof RegExp ? tillNext : header) : /\n{2}/)();
  return new ContentType({
    match: sectionRegex,
    name: name,
    short: short,
    parseContent: true,
    on: function (match) {
      var content = new Content(this, match[4] + '\n');
      content.whitespaceBefore = match[1];
      content.mark = match[2];
      content.params = match[3];
      if (this.onHeader) {
        this.onHeader.call(content, content.params);
      }
      return content;
    },
    onHeader: onHeader,
    isBlock: true,
    getText: function () {
      return this.whitespaceBefore + this.mark + ' ' + keyword + ' ' + this.params + '\n' + this.getContentText();
    }
  });
};

var Content = function (type, text) {
  if (!text) {
    text = type;
    type = null;
  }
  this.type = type;
  this.parent = null;
  this.position = null;
  this.contents = [];
  if (!type || type.parseContent) {
    var typeStub = {
      name: this.type ? this.type.name : null,
      validChildren: this.type ? this.type.validChildren : null
    };
    while (text.length > 0) {
      var found = matchNearest(typeStub, text);
      if (found.start > 0) {
        var contentBlock = Content.postCreate(new Content(Content.TextBlock, text.substring(0, found.start)), this);
        if (contentBlock) {
          this.contents.push(contentBlock);
        }
      }
      if (found.start === found.end) {
        break;
      }
      var foundContent = Content.create(found.type, found.match, this);
      if (foundContent) {
        this.contents.push(foundContent);
      }
      text = text.substr(found.end);
    }
  }
  if (this.contents.length === 0) {
    this.contents = text;
  }
  if (Array.isArray(this.contents) && this.contents.length === 1) {
    var orphan = this.contents[0];
    if (orphan.type === Content.TextBlock) {
      this.contents = orphan.contents;
    } else if (!this.type || this.type === Content.TextBlock) {
      for (var key in orphan) {
        if (orphan.hasOwnProperty(key)) {
          this[key] = orphan[key];
        }
      }
    }
  }
  if (!this.type) {
    this.type = Content.TextBlock;
  }
  if (typeof this.contents === 'string') {
    if (this.contents.startsWith('\n') && this.contents.endsWith('\n')) {
      this.contents = this.contents.replace(/^\n|\n$/g, '');
      this.isBlock = true;
    }
  }
};

Content.prototype.after = function () {
  if (Array.isArray(this.contents)) {
    for (var i = 0; i < this.contents.length; i++) {
      var element = this.contents[i];
      element.after.bind(element)();
    }
  }
};

Content.prototype.hasParent = function (parent) {
  if (!this.parent || !this.parent.type.name) {
    return false;
  }
  if (!parent) {
    return true;
  }
  var name = this.parent.type.name;
  if (typeof parent === 'string') {
    return name === parent;
  } else if (Array.isArray(parent)) {
    for (var p = 0; p < parent.length; p++) {
      if (name === parent[p]) {
        return true;
      }
    }
  }
  return false;
};

Content.prototype.position = function () {
  if (this.parent) {
    return this.parent.contents.indexOf(this);
  }
  return 0;
};

Content.prototype.getPreviousSibling = function () {
  var position = this.position();
  if (this.parent && position > 0) {
    return this.parent.contents[position - 1];
  }
  return null;
};

Content.prototype.getNextSibling = function () {
  var position = this.position();
  if (this.parent && position < this.parent.contents.length - 1) {
    return this.parent.contents[position + 1];
  }
  return null;
};

Content.prototype.getContentText = function () {
  if (typeof this.contents === 'string') {
    return this.contents;
  }
  var text = '';
  for (var i = 0; i < this.contents.length; i++) {
    text += this.contents[i].getText();
  }
  return text;
};

Content.prototype.getText = function () {
  var text = this.type.getText ? this.type.getText.bind(this)() : this.getContentText();
  if (this.isBlock || this.type.isBlock) {
    text = '\n' + text + '\n';
  }
  return text;
};

Content.prototype.getContentHtml = function () {
  if (typeof this.contents === 'string') {
    return this.contents.replace(/(^\n+)|(\n+$)/, '').replace(/\n/g, '<br/>');
  }
  var html = '';
  for (var i = 0; i < this.contents.length; i++) {
    html += this.contents[i].getHtml();
  }
  return html;
};

Content.prototype.getHtml = function () {
  if (this.type.getHtml) {
    return this.type.getHtml.bind(this)();
  }
  return this.getContentHtml();
};

Content.prototype.getTree = function () {
  var treePart = this.type.short + ': ';
  if (typeof this.contents === 'string') {
    return treePart + '"' + this.contents.replace(/\n/g, '\\n').replace(/\t/g, '\\t') + '"';
  }
  treePart += '[';
  var contents = '';
  if (this.contents.length > 0) {
    contents += '\n' + this.contents[0].getTree();
    for (var i = 1; i < this.contents.length; i++) {
      contents += ',\n' + this.contents[i].getTree();
    }
  }
  treePart += contents.replace(/\n/g, '\n  ') + '\n]';
  return treePart;
};

Content.prototype._find = function (selector, limit) {
  var found = [];
  var matches = selector.names.length === 0 || selector.names[selector.names.length - 1].name === this.type.short;
  if (matches) {
    for (var i = selector.names.length - 2, element = this.parent; i >= 0; i--, element = element.parent) {
      if (!element) {
        matches = false;
        break;
      }
      while (element.type.short !== selector.names[i].name) {
        element = element.parent;
        if (!element || selector.names[i + 1].direct) {
          matches = false;
          break;
        }
      }
      if (!matches) {
        break;
      }
    }
    if (matches) {
      for (var m = 0; m < selector.attributes.length; m++) {
        var attr = selector.attributes[m];
        if (this[attr.name] !== attr.value) {
          matches = false;
          break;
        }
      }
      if (matches) {
        found.push(this);
      }
    }
  }
  if (!selector.limit && typeof this.contents !== 'string') {
    if (limit) {
      selector.limit = true;
    }
    for (var c = 0; c < this.contents.length; c++) {
      var selectorFound = this.contents[c]._find(selector);
      for (var j = 0; j < selectorFound.length; j++) {
        found.push(selectorFound[j]);
      }
    }
  }
  return found;
};

Content.prototype.find = function (selectors) {
  selectors = selectors.split(',');
  var found = [];
  for (var i = 0; i < selectors.length; i++) {
    var firstBracket = selectors[i].indexOf('['),
      attributeRegex = / *(?:\[ *([a-zA-Z][a-zA-Z0-9]*) *(?:= *"([^"]*?)")? *])/g,
      nameRegex = / *(>?) *([a-zA-Z][a-zA-Z0-9\-]*)/g,
      limitRegex = /^ *(\|)/,
      names = [], attributes = [], index,
      limit = !!selectors[i].match(limitRegex);
    if (limit) {
      selectors[i] = selectors[i].replace(limitRegex, '');
    }

    var namesPart = firstBracket < 0 ? selectors[i] : selectors[i].substring(0, firstBracket), name;
    index = 0;
    while ((name = nameRegex.exec(namesPart)) !== null) {
      if (name.index !== index) {
        throw Error('Unrecognized syntax at index ' + index + ': ' + namesPart.substr(index));
      }
      index = nameRegex.lastIndex;
      names.push({ direct: !!name[1], name: name[2] });
    }
    if (firstBracket >= 0) {
      var attributesPart = selectors[i].substring(firstBracket), attribute;
      index = 0;
      while ((attribute = attributeRegex.exec(attributesPart)) !== null) {
        if (attribute.index !== index) {
          throw Error('Unrecognized syntax at index ' + +': ' + attributesPart.substr(index));
        }
        index = attributeRegex.lastIndex;
        attributes.push({
          name: attribute[1],
          value: attribute[2]
        });
      }
    }
    if (attributes.length !== 0 || names.length !== 0) {
      var selectorFound = this._find({
        names: names,
        attributes: attributes
      }, limit);
      for (var j = 0; j < selectorFound.length; j++) {
        found.push(selectorFound[j]);
      }
    }
  }
  return found;
};

Content.prototype.parents = function (name) {
  var p = this.parent, matches = [];
  while (p) {
    if (p.type && name === p.type.short) {
      matches.push(p);
    }
    p = p.parent;
  }
  return matches;
};

Content.prototype._findByContent = function (results, content) {
  if (typeof this.contents === 'string') {
    if (this.contents.match(content)) {
      results.push(this);
    }
  } else {
    for (var i = 0; i < this.contents.length; i++) {
      this.contents[i]._findByContent(results, content);
    }
  }
};

Content.prototype.findByContent = function (content) {
  if (!content) {
    throw new Error('findByContent: content to search for must not be null.');
  }
  if (!(content instanceof RegExp)) {
    content = new RegExp(regexUtils.escape(content));
  }
  var results = [];
  this._findByContent(results, content);
  return results;
};

Content.prototype.replaceWith = function (content) {
  if (this.parent instanceof Document) {
//TODO
  }
};

Content.postCreate = function (content, parent) {
  if (typeof content === 'string') {
    content = new Content(Content.TextBlock, content.contents);
  }
  if (parent) {
    content.parent = parent;
  }
  content.after();
  //Ignore empty text blocks
  if (content.type === Content.TextBlock && content.contents === '') {
    return null;
  }
  content._dbgname = content.type.name;
  return content;
};

Content.create = function (type, text, parent) {
  if (typeof text === 'string') {
    text = text.match(type.match);
  }
  var content = type.on(text);
  return Content.postCreate(content, parent);
};

Content.registerContentType = function (contentType, priority) {
  if (contentType instanceof ContentType) {
    if (!contentType.on) {
      throw new Error('addContentType: ContentType has missing "on" callback.');
    }
    if (!contentType.match) {
      throw new Error('addContentType: ContentType has missing "match" callback.');
    }
    if (!contentType.name) {
      throw new Error('addContentType: ContentType has missing "name" property.');
    }
    if (priority === undefined || priority > Content.Types.length) {
      Content.Types.push(contentType);
    } else {
      if (priority < 0) {
        priority = 0;
      }
      Content.Types.splice(priority, 0, contentType);
    }
    Content.TypesByName[contentType.name] = contentType;
  } else {
    throw new Error('addContentType: You passed a value that is not of a ContentType type.')
  }
};

Content.registerContentTypes = function (contentTypes, priority) {
  if (!priority) {
    priority = 0;
  }
  for (var i = 0; i < contentTypes.length; i++) {
    if (Array.isArray(contentTypes[i])) {
      priority = Content.registerContentTypes(contentTypes[i], priority);
    } else {
      Content.registerContentType(contentTypes[i], priority++);
    }
  }
  return priority;
};

Content.Types = [];
Content.TypesByName = {};

module.exports = Content;