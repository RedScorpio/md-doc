'use strict';

var fs = require('fs'),
  async = require('async'),
  DocumentExtension = require('./extension'),
  Content = require('./content'),
  ContentType = require('./content-type');

Content.registerContentTypes(require('./types'));
Content.TextBlock = Content.TypesByName['Text Block'];

var Document = function () {
  this.headers = {};
  this.contents = new Content('');
  this.contents.parent = this;
};
Document.extensions = [];

function fixWhitespace(text) {
  return text
    .replace(/\n\s+\n/g, '\n\n')
    .replace(/\n{3,}/g, '\n\n');
}

Document.prototype.readFile = function (filePath, callback) {
  if (!filePath) {
    return callback(new Error('File path must be defined.'));
  }
  var document = this;
  fs.readFile(filePath, 'utf8', function (err, text) {
    if (err) {
      return callback(err);
    }
    document.read(text, callback);
  });
};

Document.readFile = function (filePath, callback) {
  var document = new Document();
  document.readFile(filePath, callback);
};

Document.prototype.extension = function (extension) {
  if (!extension instanceof DocumentExtension) {
    throw new Error('You can register DocumentExtensions only.');
  }
  //TODO extensions per document, not global
  Document.extensions.push(extension);
  Content.registerContentTypes(extension.contentTypes, extension.priority);
};

Document.prototype.onHeadersRead = function (callback) {
  var $this = this;
  async.forEach(Document.extensions, function (ext, next) {
    ext.onDocumentHeaderRead($this, next);
  }, callback);
};

Document.prototype.read = function (text, callback) {
  if (!text) {
    callback(new Error('Text is empty. No document to read.'));
  }
  var document = this;
  process.nextTick(function () {
    //Text cleanup
    text = text
    //Standard line endings: \n
      .replace(/\r\n/g, '\n')
      .replace(/\r/g, '\n')
      //Tabs to spaces
      .replace(/\t/g, '     ')
      //Remove leading newlines
      .replace(/^\n*/g, '')
      //Multiple newlines cleanup
      .replace(/\n\s+\n/g, '\n\n')
      .replace(/\n{3,}/g, '\n\n')
      //Fix header newlines
      .replace(/(\n#{1,6}.+)(\n#{1,6}.+)/, '$1\n$2');

    //Parse headers
    var headerRegex = /^\n*([A-Z]+): *(.*)/, headerMatch;
    while (headerMatch = text.match(headerRegex)) {
      text = text.substring(headerMatch[0].length);
      document.headers[headerMatch[1]] = headerMatch[2];
    }
    document.onHeadersRead(function (err) {
      if (err) {
        return callback(err);
      }
      try {
        document.contents = new Content(Content.TypesByName['Document'], '\n' + text + '\n');
        document.contents.parent = document;
        callback(null, document);
      } catch (e) {
        callback(e);
      }
    });
  });
};

Document.read = function (text, callback) {
  var document = new Document();
  document.read(text, callback);
};
Document.prototype.getText = function () {
  var text = '';
  for (var name in this.headers) {
    if (this.headers.hasOwnProperty(name)) {
      text += name + ': ' + this.headers[name] + '\n';
    }
  }
  text += this.contents.getContentText();
  return fixWhitespace(text);
};

Document.prototype.getHtml = function () {
  return this.contents.getContentHtml();
};

Document.prototype.getTree = function () {
  return this.contents.getTree();
};
Document.prototype.find = function (selector) {
  return this.contents.find(selector);
};
Document.prototype.findByContent = function (content) {
  return this.contents.findByContent(content);
};
Document.prototype.save = function (filePath, type, callback) {
  var contentFunction;
  switch (type) {
    case 'html':
      contentFunction = function () {
        return '<html><head></head><body>' + this.getHtml() + '</body></html>';
      };
      break;
    case 'text':
    default:
      contentFunction = this.getText;
      break;
  }
  var content;
  try {
    content = contentFunction.bind(this)();
  } catch (e) {
    return callback(e);
  }
  fs.writeFile(filePath, content, 'utf8', callback);
};

Document.Extension = DocumentExtension;
Document.Content = Content;
Document.ContentType = ContentType;

module.exports = Document;