'use strict';

/**
 * Escapes string to be used in regular expression
 * @param str {string}
 * @returns {string}
 */
exports.escape = function (str) {
  return str.replace(/[\-\[\]\/\{}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
};

/**
 * Replaces parts of regular expression
 * @param regex {RegExp|string} Regex
 * @param opt {string?} Regex options
 * @returns {function(name:string|RegExp?, val:string|RegExp?):function()|RegExp}
 */
exports.replace = function (regex, opt) {
  regex = regex.source || regex;
  opt = opt || '';
  return function self(name, val) {
    if (!name) return new RegExp(regex, opt);
    val = val.source || val;
    val = val.replace(/(^|[^\[])\^/g, '$1');
    regex = regex.replace(name, val);
    return self;
  };
};