'use strict';

var ContentType = require('./content-type');

//onDocumentHeaderRead(document, lineReader, callback)
//onDocumentHeaderWrite(document, callback);

var DocumentExtension = function (name, listeners, contentTypes, priority) {
  if (!name || !listeners)
    this.name = name;
  this.onDocumentHeaderRead = listeners.onDocumentHeaderRead;
  this.priority = priority;
  if (Array.isArray(contentTypes)) {
    this.contentTypes = contentTypes;
  } else if (contentTypes instanceof ContentType) {
    this.contentTypes = [contentTypes];
  } else {
    this.contentTypes = [];
  }
};

DocumentExtension.prototype.registerContentType = function (contentType) {
  this.contentTypes.push(contentType);
};

module.exports = DocumentExtension;